#!/bin/bash
if [[ -z "$GITLAB_TOKEN" ]]; then
    GITLAB_TOKEN=$(yq '.hosts."gitlab.com".token' < ~/.config/glab-cli/config.yml)
fi

if [[ -z "$1" || -z "$2" || -z "$GITLAB_TOKEN" ]]; then
    echo "Usage: $0 <gitlab-group-url> <output-directory>"
    echo "Ensure GITLAB_TOKEN environment variable is set."
    echo "You can use CLONE_HTTP=true to use HTTP and not SSL to clone projects."
    echo "You can use CHECK_INTERVAL to custom sleep between 2 checks for export. (default 20 seconds)"
    echo "You can use USE_RCLONE=true to push to rclone datasource"
    exit 1
fi

commands=("git" "jq" "yq" "curl")

for command in "${commands[@]}"; do
    if ! command -v "$command" &> /dev/null; then
        if command -v pkgx &> /dev/null; then
            echo "pkgx is available, using pkgx $command..."
            # shellcheck disable=SC2086 # We expect globbing here
            export $command="pkgx $command"
        else
            echo "Error: pkgx is not available and $command is not installed."
            exit 1
        fi
    fi
done

export GITLAB_TOKEN
GITLAB_GROUP_URL="$1"
export OUT_DIR
OUT_DIR=$(readlink -f "$2")

export GITLAB_URL
GITLAB_URL=$(echo "$GITLAB_GROUP_URL" | awk -F"/" '{print $1"//"$3}')
# shellcheck disable=SC2295 # Expected match
GROUP_IDENTIFIER=${GITLAB_GROUP_URL#$GITLAB_URL/}
GROUP_IDENTIFIER=${GROUP_IDENTIFIER%/}

ENCODED_GROUP_IDENTIFIER=$(printf %s "$GROUP_IDENTIFIER" | jq -sRr @uri)
GROUP_ID=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$ENCODED_GROUP_IDENTIFIER" | jq -r '.id')

if [[ "$GROUP_ID" == "null" ]]; then
    echo "Error: Group identifier could not be converted to Group ID. Ensure the group identifier is correct and GITLAB_TOKEN has the necessary permissions."
    exit 1
fi

mkdir -p "$OUT_DIR"

log() {
    local msg="$1"
    local level="${2:-info}"
    local timestamp
    timestamp=$(date --rfc-3339=seconds)

    echo "[$level] [$timestamp] $msg"
}

check_project_export_status() {
    local path_with_namespace=$1
    local project_id=$2
    local export_status=""

    while [[ $export_status != "finished" ]]; do
        log "$path_with_namespace > Waiting ${CHECK_INTERVAL:-20} seconds before trying to download export…"
        sleep "${CHECK_INTERVAL:-20}"
        export_status=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$project_id/export" | jq -r '.export_status')
    done
    log "$path_with_namespace > Export successfully downloaded ✅ "
}

try_download_group_export() {
    local path=$1
    local group_id=$2
    local local_out_dir=$3
    local download_success=false

    while [[ $download_success == false ]]; do
        log "$path > Waiting ${CHECK_INTERVAL:-20} seconds before trying to download…"
        sleep "${CHECK_INTERVAL:-20}"
        curl -s --location -o "$local_out_dir/$path.export.tar.gz" --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/export/download"
        if [[ $? -eq 0 && -f "$local_out_dir/$path.export.tar.gz" ]]; then
            download_success=true
            log "$path > Export successfully downloaded ✅ "
        fi
    done
}

process_project() {
    local project_id=$1
    local project_name=$2
    local project_url=$3
    local path_with_namespace=$4
    local path=$5
    local ssh_url_to_repo=$6
    local http_url_to_repo=$7
    local wiki_access_level=$8
    log "$path_with_namespace > Processing project ($project_url)…"

    if [[ "$CLONE_HTTP" == "true" ]]; then
        local url_to_repo=$http_url_to_repo
    else
        local url_to_repo=$ssh_url_to_repo
    fi

    if [[ "$wiki_access_level" == "enabled" || "$wiki_access_level" == "private" ]]; then
        local wiki_enabled=true
    else
        local wiki_enabled=false
    fi

    local local_out_dir="$OUT_DIR/$path_with_namespace"

    project_temp_dir=$(mktemp -d -t "$project_name.XXXXXX")
    mkdir -p "$local_out_dir"

    log "$path_with_namespace > Cloning git…"
    git clone --quiet --mirror "$url_to_repo" "$project_temp_dir/repo" || log "$path_with_namespace > Ignoring empty git"
    git -C "$project_temp_dir/repo" bundle create --quiet "$local_out_dir/$path.bundle" --all || log "$path_with_namespace > Ignoring empty git"
    if [[ "$USE_RCLONE" == "true" && -f "$local_out_dir/$path.bundle" ]]; then
      rclone copy "$local_out_dir/$path.bundle" "default:$BUCKET/$RCLONE_SUBDIRECTORY/$path_with_namespace/"
    fi
    log "$path_with_namespace > Git backup done ✅ "

    if [[ "$wiki_enabled" == true ]]; then
      log "$path_with_namespace > Wiki found, cloning…"
      git clone --quiet --mirror "${url_to_repo%\.git}.wiki.git" "$project_temp_dir/wiki" || log "$path_with_namespace > Ignoring empty wiki"
      git -C "$project_temp_dir/wiki" bundle create --quiet "$local_out_dir/$path.wiki.bundle" --all || log "$path_with_namespace > Ignoring empty wiki"
      if [[ "$USE_RCLONE" == "true" && -f "$local_out_dir/$path.wiki.bundle" ]]; then
        rclone copy "$local_out_dir/$path.wiki.bundle" "default:$BUCKET/$RCLONE_SUBDIRECTORY/$path_with_namespace/"
      fi
      log "$path_with_namespace > Wiki cloning done ✅ "
    fi

    rm -rf "$project_temp_dir"
    log "$path_with_namespace > Triggering export…"
    curl -s -o /dev/null --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$project_id/export"
    check_project_export_status "$path_with_namespace" "$project_id"

    curl -s --location -o "$local_out_dir/$path.export.tar.gz" --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/projects/$project_id/export/download"
    if [[ "$USE_RCLONE" == "true" && -f "$local_out_dir/$path.export.tar.gz" ]]; then
      rclone copy "$local_out_dir/$path.export.tar.gz" "default:$BUCKET/$RCLONE_SUBDIRECTORY/$path_with_namespace/"
    fi
}

process_group() {
    local group_id=$1
    group_temp_dir=$(mktemp -d -t "$group_id.XXXXXX")

    # shellcheck disable=SC2046 # We expect word splitting here
    group_data=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id")
    path=$(echo "$group_data" | jq -r '.path')
    group_url=$(echo "$group_data" | jq -r '.web_url')
    group_wiki_access_level=$(echo "$group_data" | jq -r '.wiki_access_level')
    group_path=$(echo "${group_url/\/groups\//\/}" | sed 's|^[^:]*://[^/]*||' | sed 's|^/||')
    log "$path > Processing group…"
    mkdir -p "$group_path"

    local wiki_enabled=false
    if [[ "$group_wiki_access_level" == "enabled" || "$group_wiki_access_level" == "private" ]]; then
        local wiki_enabled
    fi
    local url_to_repo
    if [[ "$CLONE_HTTP" == "true" ]]; then
        url_to_repo="$group_url"
    else
        url_to_repo="git@$(echo "$group_url" | awk -F'/' '{print $3}'):$group_path"
    fi

    local local_out_dir=$OUT_DIR/$group_path

    if [[ "$wiki_enabled" == true ]]; then
      log "$path > Wiki found, cloning…"
      git clone --quiet --mirror "${url_to_repo}.wiki.git" "$group_temp_dir/wiki" || log "$path > Ignoring empty wiki"
      git -C "$group_temp_dir/wiki" bundle create --quiet "$local_out_dir/$path.wiki.bundle" --all || log "$path > Ignoring empty wiki"
      if [[ "$USE_RCLONE" == "true" && -f "$local_out_dir/$path.wiki.bundle" ]]; then
        rclone copy "$local_out_dir/$path.wiki.bundle" "default:$BUCKET/$RCLONE_SUBDIRECTORY/$group_path/"
      fi
      log "$path > Wiki cloning done ✅ "
    fi

    log "$path > Triggering export…"
    curl -s -o /dev/null --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/export"
    try_download_group_export "$path" "$group_id" "$local_out_dir"
    if [[ "$USE_RCLONE" == "true" && -f "$local_out_dir/$path.export.tar.gz" ]]; then
      rclone copy "$local_out_dir/$path.export.tar.gz" "default:$BUCKET/$RCLONE_SUBDIRECTORY/$group_path/"
    fi
    subgroups=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/subgroups" | jq -r '.[] | .id')
    projects=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_URL/api/v4/groups/$group_id/projects" | jq -r '.[] | [.id, (.name | split(" ") | join("_")), .web_url, .path_with_namespace, .path, .ssh_url_to_repo, .http_url_to_repo, .wiki_access_level] | @tsv')

    if [[ -n "$projects" ]]; then
      log "$projects" | while IFS=$'\t' read -r project_id project_name project_url path_with_namespace project_path ssh_url_to_repo http_url_to_repo wiki_access_level
      do
        process_project "$project_id" "$project_name" "$project_url" "$path_with_namespace" "$project_path" "$ssh_url_to_repo" "$http_url_to_repo" "$wiki_access_level"
      done
    else
      log "$path > Don't have any project"
    fi


    log "$path > Processing group done ✅ "
    for subgroup_id in $subgroups; do
        process_group "$subgroup_id"
    done
}

process_group "$GROUP_ID"
