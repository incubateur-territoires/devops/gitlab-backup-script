FROM debian:stable-slim
WORKDIR /opt/backup

RUN mkdir -p /opt/backup
RUN apt-get update && apt-get install -y \
    git \
    curl \
    yq \
    jq \
    parallel \
    rclone \
 && rm -rf /var/lib/apt/lists/*
COPY --link --chmod=500 ./backup.sh /

ENTRYPOINT ["/backup.sh"]
