# GitLab Group Backup Script

This script is designed to comprehensively back up all repositories, wikis, and project/export data from a specified GitLab group, including all subgroups and their projects. 
It is tailored for a system where the `git`, `jq`, `yq`, `parallel`, and `curl` utilities are available.
It can use pkgx if available.

## Features

- Clones and backs up all Git repositories and associated wikis within the specified GitLab group and its subgroups.
- Creates Git bundle files for each repository and wiki, allowing for efficient storage and transportation.
- Triggers and downloads project exports for all projects, encapsulating issues, merge requests, and other GitLab-specific data.
- Supports parallel processing of multiple projects for faster backup operations.
- Provides configurable options for using HTTP/SSL, setting parallel job count, and customizing check interval between export status checks.

## Backup Coverage

The following data is covered in the backup:

1. **Git Repositories**:
    - All branches, tags, and commit history.
    - Git LFS objects.

2. **Wikis**:
    - All wiki pages and their history.

3. **Project Exports**:
    - Issues and merge requests data.
    - Labels, milestones, snippets, and other project metadata.
    - CI/CD pipelines and variables.
    - Artifacts and other generated data.

## Usage

```bash
Usage: script.sh <gitlab-group-url> <output-directory>
```

### Parameters

- `<gitlab-group-url>`: The URL of the GitLab group you want to back up.
- `<output-directory>`: The directory where you want to save the backup data.

### Environment Variables

- `GITLAB_TOKEN`: Your GitLab personal access token. If not set, the script will attempt to extract it from `~/.config/glab-cli/config.yml`.
- `PARALLEL_JOBS`: (Optional) The number of projects to process in parallel. Defaults to 5.
- `CLONE_HTTP`: (Optional) Set to `true` to use HTTP instead of SSH for cloning projects. Defaults to `false` (SSH).
- `CHECK_INTERVAL`: (Optional) The sleep interval between checks for export readiness in seconds. Defaults to 20 seconds.

## Execution

1. Ensure you have the necessary permissions and a GitLab personal access token with the required scopes.
2. Run the script, providing the GitLab group URL and the directory where you want to save the backup data.

```bash
./backup.sh https://gitlab.com/groups/my-group /path/to/backup
```

## Notes

- Ensure that the machine where the script is running has sufficient storage space to accommodate all backup data.
- The script will create a directory structure mirroring the GitLab group, subgroup, and project hierarchy within the specified output directory.
- The script will generate `.bundle` files for repositories and wikis, and `.tar.gz` files for project exports.
- Ensure that your GitLab token has the necessary permissions to access all desired data and trigger project exports.
